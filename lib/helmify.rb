require 'csv'
require 'yaml'
require 'active_support/core_ext/string'
require 'logger'

require "helmify/version"
require "helmify/helm_wrapper"
require "helmify/helm_chart"
require "helmify/helm_repo"
require "helmify/helm_release"

module Helmify

  Logger = Logger.new(STDOUT)

  def self.to_indexed_hash(array)
    array.each_with_index.reduce({}) do |accu, (h_v, idx)|
      accu.merge(idx => h_v)
    end
  end

  def self.flatten_hash(hash, separtor = ".")
    hash.each_with_object({}) do |(k, v), h|
      if v.is_a? Hash
        flatten_hash(v, separtor).map do |h_k, h_v|
          h["#{k.to_s}#{separtor}#{h_k.to_s}".to_s] = h_v
        end
      elsif v.is_a? Array
        flatten_hash(to_indexed_hash(v), separtor).map do |h_k, h_v|
          h["#{k.to_s}#{separtor}#{h_k.to_s}".to_s] = h_v
        end
      else
        h[k] = v
      end
     end
  end

  def self.debug(message)
    Helmify::Logger.debug message
  end
end
