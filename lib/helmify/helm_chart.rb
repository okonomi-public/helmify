class HelmChart
  attr_reader :name, :full_name, :description, :version, :app_version, :repo

  def initialize(full_name, description, version, app_version, repo)
    @full_name, @description, @version, @app_version, @repo = full_name, description, version, app_version, repo
    @name = @full_name.split('/').last
  end

  def install(release_name: nil, namespace: 'default', values: {})
    HelmRelease.new(name: release_name, namespace: namespace, chart_name: full_name, chart_version: version).tap do |release|
      release.install(values)
    end
  end

  def self.find(full_name)
    repo_name, name = full_name.split('/')

    repo = HelmRepo.find(repo_name)

    return nil if repo.nil?

    repo.chart(name)
  end
end
