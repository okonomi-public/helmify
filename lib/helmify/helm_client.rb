require 'csv'

class HelmClient

  def installed?(release_name, namespace)
    installed_release(release_name, namespace).present?
  end

  def installed_chart_version(release_name, namespace)
    chart = (installed_release(release_name, namespace) || {})[:chart]
    return nil if chart.blank?

    chart.split(/\-/).last
  end

  def installed_release(release_name, namespace)
    ls(namespace).find do |item|
      item[:name] == sanitize_name(release_name) && item[:namespace] == namespace
    end
  end

  def install(release_name, namespace, chart, values)
    apply("install", release_name, namespace, chart, values)
  end

  def upgrade(release_name, namespace, chart, values)
    apply("upgrade", release_name, namespace, chart, values)
  end

  def apply(action, release_name, namespace, chart, values, dry_run = false)
    write_values(values)

    ensure_chart chart

    segments = [
      action,
      sanitize_name(release_name),
      chart[:name],
      "-n", namespace,
      "-f", values_file
    ]

    if dry_run
      segments += ["--dry-run"]
    end

    if chart[:version]
      segments += ["--version", chart[:version]]
    end

    p segments

    result = run(*segments)

    p result

    if !result[:success]
      raise "Failed to #{action} #{release_name}"
    end

    result
  end

  def delete(release_name, namespace)
    run("delete", sanitize_name(release_name), "-n", namespace)
  end

  def ls(namespace)
    result = run("ls", "-n", namespace)

    if result[:success]
      parse_output(result[:out]).map(&:to_h).map do |release|
        release.merge(updated: Time.parse(release[:updated]))
      end
    else
      []
    end
  end

  def values(release_name, namespace)
    result = run("get", "values", release_name, "-n", namespace)

    if result[:success]
      output = result[:out].gsub("USER-SUPPLIED VALUES:\n", "").strip
      YAML.load(output)
    else
      nil
    end
  end

  def ensure_chart(definition)
    if !definition[:repo].nil?
      run "repo", "add", definition[:repo][:name], definition[:repo][:url]
      run "repo", "update"
    end
  end

  def run(*args)
    cmd = (["helm"] + args).join(' ')

    puts "[Helm] run `#{cmd}`"

    out = `#{cmd}`

    {
      pid: $?.pid,
      success: $?.success?,
      out: out,
      existstatus: $?.exitstatus
    }
  end

  def parse_output(output)
    CSV.parse(output, :col_sep => "\t", headers: true, header_converters: lambda { |h| h.strip.downcase.underscore.to_sym }, converters: lambda { |h| h.strip })
  end

  def write_values(values)
    FileUtils.mkdir_p values_folder
    File.open(values_file, 'w') {|f| f.write YAML.dump(values.to_hash.deep_stringify_keys) }
  end

  def values_file
    @values_file ||= "#{values_folder}/#{(Time.now.to_f * 100000).to_s}.yaml"
  end

  def values_folder
    @values_folder ||= '/tmp'
  end

  def sanitize_name(name)
    name.gsub(/\./, '-')
  end
end
