class HelmRelease

  attr_reader :name, :namespace, :chart_name, :chart_version

  def initialize(name:, namespace: 'default', chart_name:, chart_version: nil)
    @name, @namespace, @chart_name, @chart_version = name, namespace, chart_name, chart_version
    @name ||= chart_name.split('/').last
  end

  def installed?
    !HelmRelease.list(namespace).find { |r| r["name"] == name }.nil?
  end

  def install(values = {})
    apply('install', values)
    self
  end

  def upgrade(values = {})
    apply('upgrade', values)
    self
  end

  def delete
    result = HelmWrapper.run("delete", name, "-n", namespace)
    if result[:success]
      self
    else
      false
    end
  end

  def patch(params = {})
    old_values = values
    new_values = old_values.deep_merge(params.deep_stringify_keys)

    if old_values != new_values
      Helmify.debug "[Helm] patching #{namespace}/#{name}"

      upgrade(new_values)
    else
      puts "[Helm] #{namespace}/#{name} is up to date"
    end

    self
  end

  def values
    result = HelmWrapper.run("get", "values", name, "-n", namespace)

    if result[:success]
      output = result[:output].gsub("USER-SUPPLIED VALUES:\n", "").strip
      YAML.load(output) || {}
    else
      {}
    end
  end

  def valid_chart?
    chart_name.present?
  end

private

  def self.list(namespace = nil)
    HelmWrapper.query(*list_cmd(namespace)).map do |release|
      release
    end
  end

  def self.list_cmd(namespace)
    if namespace.nil?
      ['list', '--all-namespaces']
    else
      ['list', '-n', namespace]
    end
  end

  def apply(action, values = {})
    raise "Chart name and version must be provided" unless valid_chart?

    flattened_values = Helmify.flatten_hash(values)

    segments = [
      action,
      sanitized_name,
      chart_name,
      "-n", namespace
    ]

    if !flattened_values.empty?
      segments += ["--set", flattened_values.map { |(k,v)| [k,v].join('=') }.join(',')]
    end

    if chart_version.present?
      segments += ["--version", chart_version]
    end

    result = HelmWrapper.run(*segments)

    if !result[:success]
      Helmify.debug "[Helm] #{action} #{namespace}/#{name} failed"
      raise "Failed to #{action} #{name}"
    else
      Helmify.debug "[Helm] #{action} #{namespace}/#{name} succeeded"
    end

    result
  end

  def sanitized_name
    name.gsub(/\./, '-')
  end
end
