class HelmRepo
  attr_reader :name, :url

  def initialize(name, url)
    @name, @url = name, url
  end

  def charts(reload = false)
    @charts = nil if reload
    @charts ||= load_charts
  end

  def chart(name, reload=false)
    charts(reload).find do |chart|
      chart.name == name
    end
  end

  def update
    HelmWrapper.run "repo", "update"
  end

  def self.find(name)
    list.find { |repo| repo.name == name }
  end

  def self.list(reload = false)
    @repos = nil if reload
    @repos ||= HelmWrapper.query('repo', 'list').map do |item|
      new(item["name"], item["url"])
    end
  end

private

  def load_charts
    HelmWrapper.query('search', 'repo', name).map do |d|
      HelmChart.new(d["name"], d["description"], d["version"], d["app_version"], self)
    end
  end

end
