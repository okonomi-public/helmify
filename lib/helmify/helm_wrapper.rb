class HelmWrapper
  def self.run(*args)
    cmd = (["helm"] + args).join(' ')

    Helmify.debug "[Helm] run `#{cmd}`"

    out = `#{cmd}`

    {
      pid: $?.pid,
      success: $?.success?,
      output: out,
      existstatus: $?.exitstatus
    }
  end

  def self.query(*args)
    result = run(args + ['-o', 'yaml'])

    if result[:success]
      YAML.load(result[:output])
    else
      raise "Failed querying helm: '#{args.join(' ')}'"
    end
  end
end
